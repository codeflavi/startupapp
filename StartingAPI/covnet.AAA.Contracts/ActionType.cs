﻿using System;
using System.Collections.Generic;
using System.Text;

namespace covnet.AAA.Contracts
{
    public enum ActionType
    {
        ViewMetadata,
        View,
        Create,
        Edit,
        Delete,
        Execute,
        Link
    }
}

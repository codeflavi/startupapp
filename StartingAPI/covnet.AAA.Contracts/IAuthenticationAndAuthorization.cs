﻿using covnet.Models.EfCoreModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace covnet.AAA.Contracts
{
    public interface IAuthenticationAndAuthorization
    {
        Task<User> GetCurrentUserAsync();

        User GetCurrentUser();

        int GetSharedOrganisationId();

        Task<bool> IsAuthorizedAsync<T>(
            User user,
            T resource,
            AuthorizationConfiguration configuration)
            where T : class;

        Task<bool> IsCurrentUserAuthorizedAsync<T>(
            T resource,
            AuthorizationConfiguration configuration)
            where T : class;

        Task AuthorizeAsync<T>(
            User user,
            T resource,
            AuthorizationConfiguration configuration)
            where T : class;

        Task AuthorizeForCurrentUserAsync<T>(
            T resource,
            AuthorizationConfiguration configuration)
            where T : class;

        Task AuthorizeForCurrentUserAsync<T>(
            IEnumerable<T> resources,
            AuthorizationConfiguration configuration)
            where T : class;

        Task InternalAuthorizeAsync<T>(
            User user,
            T resource,
            IAuthorizationContext context,
            AuthorizationConfiguration configuration)
            where T : class;

        Expression<Func<T, bool>> GetAuthorizationExpressionForUser<T>(
            User user,
            AuthorizationConfiguration configuration)
            where T : class;

        /*Only use this method when you expect and want the predicate to be null in case
         the user role has no access at all to a specific object. If you do not check
         or need the check to see if predicate is null, use the 'GetAuthorizationExpressionForUser'
         method which throws unauthorised exception*/
        Task<Expression<Func<T, bool>>> TryGetEditAuthorisationExpressionForCurrentUserAsync<T>(
             AuthorizationConfiguration configuration)
             where T : class;
    }
}

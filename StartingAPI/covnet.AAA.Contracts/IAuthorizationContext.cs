﻿namespace covnet.AAA.Contracts
{
    public interface IAuthorizationContext
    {
        bool Add(object resource);
    }
}

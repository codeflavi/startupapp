﻿using covnet.Models.EfCoreModels;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace covnet.AAA.Contracts
{
    public interface IAuthorizationProvider
    {
        Task AuthorizeAsync(
            User user,
            object resource,
            IAuthorizationContext context,
            AuthorizationConfiguration configuration);
    }
}

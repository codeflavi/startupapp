﻿using covnet.Models.EfCoreModels;
using System;

namespace covnet.AAA.Contracts
{
    public interface IClaimsVerfication
    {
        Guid GetUserGuid();

        bool CanAccessAPI();

        int GetAnalyticsUserId();

        User GetAnalyticsUser();
    }
}

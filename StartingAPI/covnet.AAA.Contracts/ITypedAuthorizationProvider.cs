﻿using covnet.Models.EfCoreModels;
using System;
using System.Linq.Expressions;

namespace covnet.AAA.Contracts
{
    public interface ITypedAuthorizationProvider<T> : IAuthorizationProvider where T : class
    {
        Expression<Func<T, bool>> GetAuthorizationExpressionForUser(
            User user,
            AuthorizationConfiguration configuration);
    }
}

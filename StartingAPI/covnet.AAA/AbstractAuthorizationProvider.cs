﻿using covnet.AAA.Contracts;
using covnet.Models;
using covnet.Models.EfCoreModels;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace covnet.AAA
{
    public abstract class AbstractAuthorizationProvider<T> : ITypedAuthorizationProvider<T>
        where T : class
    {
        protected readonly IAuthenticationAndAuthorization AuthorizationService;

        public AbstractAuthorizationProvider(IAuthenticationAndAuthorization s)
            => AuthorizationService = s;
        
        Task IAuthorizationProvider.AuthorizeAsync(
            User user, 
            object resource,
            IAuthorizationContext context,
            AuthorizationConfiguration configuration)
            => AuthorizeAsync(user, (T)resource, context, configuration);
       
        protected abstract Task AuthorizeAsync(User user, T t, 
            IAuthorizationContext context, AuthorizationConfiguration configuration);

        Expression<Func<T, bool>> ITypedAuthorizationProvider<T>.GetAuthorizationExpressionForUser(
            User user,
            AuthorizationConfiguration configuration)
        => GetAuthorizationExpressionForUser(user,configuration);

        protected abstract Expression<Func<T,bool>> GetAuthorizationExpressionForUser(
            User user,
            AuthorizationConfiguration configuration);
    }
}

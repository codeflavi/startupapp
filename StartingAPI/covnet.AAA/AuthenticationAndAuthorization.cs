﻿using covnet.AAA.Contracts;
using covnet.Models.EfCoreModels;
using covnet.Repository.Contracts;
using covnet.Shared.Exceptions;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace covnet.AAA
{
    public sealed class AuthenticationAndAuthorization : IAuthenticationAndAuthorization
    {
        private readonly IHttpContextAccessor _accessor;
        private readonly IServiceProvider _serviceProvider;

        private User _user = default(User);
        private int _sharedOrganisationId = 0;

        public AuthenticationAndAuthorization(
            IHttpContextAccessor accessor,
            IServiceProvider serviceProvider)
        {
            _accessor = accessor;
            _serviceProvider = serviceProvider;
        }

        public Task<User> GetCurrentUserAsync()
        {
            return Task.FromResult(GetCurrentUser());
        }

        public User GetCurrentUser()
        {
            if (_user == default(User))
            {
                var jsonUser = _accessor.HttpContext?.User.Claims
                    .FirstOrDefault(x => x.Type == ClaimsVerification.CLAIM_TYPE_USER_DETAILS)?.Value;

                _user = (jsonUser != null) ? Newtonsoft.Json.JsonConvert.DeserializeObject<User>(jsonUser) : null;
            }

            return _user;
        }

        public int GetSharedOrganisationId()
        {
            if (_sharedOrganisationId == 0)
            {
                string sharedOrId = _accessor
                    .HttpContext?.User.Claims
                    .FirstOrDefault(x => x.Type == ClaimsVerification.CLAIM_TYPE_SHARED_ORGANISATION_ID)?.Value;

                _sharedOrganisationId = (sharedOrId != null) ? int.Parse(sharedOrId) : 0;
            }

            return _sharedOrganisationId;
        }

        public Task<bool> IsCurrentUserAuthorizedAsync<T>(
            T resource,
            AuthorizationConfiguration configuration)
            where T : class
        {
            return IsAuthorizedAsync(GetCurrentUser(), resource, configuration);
        }

        public async Task<bool> IsAuthorizedAsync<T>(
            User user,
            T resource,
            AuthorizationConfiguration configuration)
            where T : class
        {
            try
            {
                await AuthorizeAsync(user, resource, configuration);
                return true;
            }
            catch (UnauthorizedException)
            {
                return false;
            }
        }

        public Task AuthorizeForCurrentUserAsync<T>(
            T resource,
            AuthorizationConfiguration configuration)
            where T : class
            => AuthorizeForCurrentUserAsync(
                (IEnumerable<T>)new System.Collections.ObjectModel.ReadOnlyCollection<T>(new T[] { resource }),
                configuration);

        public async Task AuthorizeForCurrentUserAsync<T>(
            IEnumerable<T> resources,
            AuthorizationConfiguration configuration)
            where T : class
        {
            User currentUser = await GetCurrentUserAsync();
            if (resources != null)
            {
                foreach (T r in resources)
                {
                    await AuthorizeAsync(currentUser, r, configuration);
                }
            }
        }

        public Task AuthorizeAsync<T>(
            User user,
            T resource,
            AuthorizationConfiguration configuration)
            where T : class
        {
            return InternalAuthorizeAsync(user, resource,
                new AuthorizationContext(), configuration);
        }

        public async Task InternalAuthorizeAsync<T>(
            User user,
            T resource,
            IAuthorizationContext context,
            AuthorizationConfiguration configuration)
            where T : class
        {
            if (resource != null)
            {
                if (user == default(User))
                {
                    throw new UnauthorizedException($"User == null does not have access to resource {resource}.");
                }

                // if not added to the set then it means it is already under verification
                if (context.Add(resource))
                {
                    var authenticationService = GetCompatibleService<ITypedAuthorizationProvider<T>>();

                    if (authenticationService != default(ITypedAuthorizationProvider<T>))
                    {
                        if (configuration?.ActionType != ActionType.Create)
                        {
                            // If there isn't a CRUD repository for T, then just use the resource as is
                            resource = await (GetCompatibleService<ICrudRepository<T>>()?.EnsureAuthorizationDataAsync(resource) ?? Task.FromResult(resource));
                        }

                        await authenticationService.AuthorizeAsync(user, resource, context, configuration);
                    }
                    else
                    {
                        throw new UnauthorizedException($"User {user.Id} does not have access to resource {resource}. There is no AuthorizationProvider for this type of resource.");
                    }
                }
            }
        }

        // TODO In case of performance issues, we may want to get rid of this method
        // and manually add all required services of ICrudRepository<T> type in Startup
        private S GetCompatibleService<S>()
            where S : class
        {
            S service = (S)_serviceProvider?.GetService(typeof(S)) ?? default(S);
            if (service != null)
            {
                return service;
            }
            else
            {
                // Using reflection find all subclasses of S
                // and then get the first available service 
                try
                {
                    return Assembly
                        .GetAssembly(typeof(S))
                        .GetTypes()
                        .Where(t => typeof(S).IsAssignableFrom(t) && t != typeof(S))
                        .Select(t => (S)_serviceProvider.GetService(t))
                        .First(s => !s.Equals(default(S)));
                }
                catch (Exception)
                {
                    return default(S);
                }
            }
        }

        public Expression<Func<T, bool>> GetAuthorizationExpressionForUser<T>(
            User user,
            AuthorizationConfiguration configuration)
            where T : class
        {
            var authenticationService = GetCompatibleService<ITypedAuthorizationProvider<T>>();

            return authenticationService.GetAuthorizationExpressionForUser(user, configuration);
        }

        public async Task<Expression<Func<T, bool>>> TryGetEditAuthorisationExpressionForCurrentUserAsync<T>(
            AuthorizationConfiguration configuration) 
            where T : class
        {
            User currentUser = await GetCurrentUserAsync();
            Expression<Func<T, bool>> predicate;

            try
            {
                predicate = GetAuthorizationExpressionForUser<T>(
                    currentUser,
                    configuration);
            }
            catch (UnauthorizedException)
            {
                predicate = null;
            }

            return predicate;
        }
    }
}

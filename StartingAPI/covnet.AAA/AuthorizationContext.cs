﻿using covnet.AAA.Contracts;
using covnet.Models.Base;
using System.Collections.Generic;

namespace covnet.AAA
{
    internal class AuthorizationContext : IAuthorizationContext
    {
        public HashSet<string> runningAuthorizations = new HashSet<string>();

        public AuthorizationContext()
        {
        }

        public bool Add(object resource)
        {
            if (resource is IWithObjectId rid)
            { 
                return runningAuthorizations.Add($"{resource.GetType()}{rid.ObjectId}");
            }

            return runningAuthorizations.Add(resource.GetHashCode().ToString());
        }
    }
}

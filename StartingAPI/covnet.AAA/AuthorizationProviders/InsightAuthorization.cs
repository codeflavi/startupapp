﻿//Example class
//using covnet.AAA.Contracts;
//using covnet.Models;
//using covnet.Models.EfCoreModels;
//using covnet.Shared;
//using covnet.Shared.Exceptions;
//using System;
//using System.Linq;
//using System.Linq.Expressions;
//using System.Threading.Tasks;

//namespace covnet.AAA.AuthorizationProviders
//{
//    public class InsightAuthorization : AbstractAuthorizationProvider<Insight>
//    {
//        public InsightAuthorization(IAuthenticationAndAuthorization s)
//            : base(s)
//        {
//        }

//#pragma warning disable 1998
//        protected async override Task AuthorizeAsync(
//            /*[NotNull]*/ User user,
//            /*[NotNull]*/ Insight resource,
//            IAuthorizationContext context,
//            AuthorizationConfiguration configuration)
//        {
//            Preconditions.ThrowIfNull(user, "user");
//            Preconditions.ThrowIfNull(resource, "resource");

//            Expression<Func<Insight, bool>> predicate = null;
//            predicate = GetAuthorizationExpressionForUser(user, configuration);

//            if (!predicate.Compile()(resource))
//            {
//                throw new UnauthorizedException($"User {user.Name} does not have access to the " +
//                    $"insight with id = {resource.ObjectId}. Action type: {configuration.ActionType}");
//            }

//            if (configuration.ActionType == ActionType.Create
//                || configuration.ActionType == ActionType.Edit)
//            {

//                AuthorizationConfiguration configurationLink = new AuthorizationConfiguration
//                {
//                    ActionType = ActionType.Link
//                };

//                if (resource.DataViewId.HasValue)
//                {
//                    if (resource.DataView == default(DataView))
//                    {
//                        resource.DataView = new DataView
//                        {
//                            ObjectId = (int)resource.DataViewId
//                        };
//                    }

//                    await AuthorizationService.InternalAuthorizeAsync(user, resource.DataView, context, configurationLink);
//                }
//            }
//        }

//        protected override Expression<Func<Insight, bool>> GetAuthorizationExpressionForUser(
//            User user,
//            AuthorizationConfiguration configuration)
//        {
//            Expression<Func<Insight, bool>> predicate = null;
//            int sharedOrgId = AuthorizationService.GetSharedOrganisationId();

//            if (user.RoleType == RoleType.Editor)
//            {
//                if (configuration.ActionType == ActionType.Link ||
//                   configuration.ActionType == ActionType.Create)
//                {
//                    predicate = ins => ins.OrganisationOwnerId == user.OrganisationId;
//                }
//                else
//                {
//                    predicate = ins => ins.Active && ins.OrganisationOwnerId == user.OrganisationId;
//                }

//            }
//            else if (user.RoleType == RoleType.Administrator)
//            {
//                if (configuration.ActionType == ActionType.View
//                   || configuration.ActionType == ActionType.Execute
//                   || configuration.ActionType == ActionType.ViewMetadata
//                   || configuration.ActionType == ActionType.Link)
//                {
//                    /*SELECT[ins].[ObjectID], [ins].[CrDate], [ins].[CrUser], [ins].[DataViewId], [ins].[Description], [ins].[ExternalId], [ins].[ModDate], [ins].[ModUser], [ins].[Name], [ins].[OrganisationOwnerId], [ins].[ReportDef], [ins].[ReportType], [ins].[template], [ins].[UsingSimpleEditor]
//                      FROM[Insights] AS[ins]
//                      WHERE([ins].[OrganisationOwnerId] = @__currentUser_OrganisationId_0) OR(([ins].[OrganisationOwnerId] = @__sharedOrgId_1) AND EXISTS(
//                          SELECT 1
//                          FROM[OrganisationInsights] AS [_]
//                          WHERE (([_].[OrganisationId] = @__currentUser_OrganisationId_0) AND([_].[InsightId] = [ins].[ObjectID])) AND([ins].[ObjectID] = [_].[InsightId]))) */

//                    predicate = ins => ins.Active && //insight is active
//                                       (ins.OrganisationOwnerId == user.OrganisationId // insight is in users org OR
//                                    || (ins.OrganisationOwnerId == sharedOrgId // insight is in shared org
//                                         && ins.OrganisationInsights //AND insight has been provisioned to user org
//                                                .Any(_ => _.OrganisationId == user.OrganisationId
//                                                        && _.InsightId == ins.ObjectId)));

//                }
//            }
//            else if (user.RoleType == RoleType.AdministratorDesigner)
//            {
//                if (configuration.ActionType == ActionType.View
//                    || configuration.ActionType == ActionType.Execute
//                    || configuration.ActionType == ActionType.ViewMetadata)
//                {
//                    predicate = ins => ins.Active && //insight is active
//                                        (ins.OrganisationOwnerId == user.OrganisationId // insight is in users org OR
//                                    || (ins.OrganisationOwnerId == sharedOrgId // insight is in shared org
//                                         && ins.OrganisationInsights //AND insight has been provisioned to user org
//                                                .Any(_ => _.OrganisationId == user.OrganisationId
//                                                        && _.InsightId == ins.ObjectId)));
//                }
//                else if (configuration.ActionType == ActionType.Link)
//                {
//                    predicate = ins => ins.OrganisationOwnerId == user.OrganisationId // insight is in users org OR
//                                   || (ins.OrganisationOwnerId == sharedOrgId // insight is in shared org
//                                        && ins.OrganisationInsights //AND insight has been provisioned to user org
//                                               .Any(_ => _.OrganisationId == user.OrganisationId
//                                                       && _.InsightId == ins.ObjectId));
//                }
//                else if (configuration.ActionType == ActionType.Create)
//                {
//                    //admin designers can only create/link ins that are in their org
//                    //but we do not need to check the active flag
//                    predicate = ins => ins.OrganisationOwnerId == user.OrganisationId;
//                }
//                else if (configuration.ActionType == ActionType.Edit ||
//                         configuration.ActionType == ActionType.Delete)
//                {
//                    //admin designers can only modify ins that are in their org
//                    predicate = ins => ins.Active && ins.OrganisationOwnerId == user.OrganisationId;
//                    // TODO To be uncommented upon clarification provided
//                    // - should Admins (incl. AdminDesingers) have access only to import-based insights?
//                    // - for all operations or only for Create/Update/Delete, while Read is granted to all insights?
//                    //&& (ins.DataView != default(DataView) && ins.DataView.ImportId.HasValue); // insight is import based
//                }
//            }
//            else if (user.RoleType == RoleType.Viewer)
//            {
//                if (configuration.ActionType == ActionType.View
//                   || configuration.ActionType == ActionType.Execute
//                   || configuration.ActionType == ActionType.ViewMetadata)
//                {
//                    /*SELECT [ins].[ObjectID], [ins].[CrDate], [ins].[CrUser], [ins].[DataViewId], [ins].[Description], [ins].[ExternalId], [ins].[ModDate], [ins].[ModUser], [ins].[Name], [ins].[OrganisationOwnerId], [ins].[ReportDef], [ins].[ReportType], [ins].[template], [ins].[UsingSimpleEditor]
//                      FROM [Insights] AS [ins]
//                      WHERE (([ins].[OrganisationOwnerId] = @__currentUser_OrganisationId_0) AND EXISTS (
//                          SELECT 1
//                          FROM [InsightRoles] AS [insRole]
//                          INNER JOIN [BA_Role] AS [insRole.RoleO] ON [insRole].[RoleOID] = [insRole.RoleO].[ObjectID]
//                          WHERE (EXISTS (
//                              SELECT 1
//                              FROM [BA_UserRole] AS [usrRole]
//                              WHERE ([usrRole].[UserId] = @__currentUser_ObjectId_1) AND ([insRole.RoleO].[ObjectID] = [usrRole].[RoleOID])) AND ([insRole.RoleO].[OrganisationId] = @__currentUser_OrganisationId_0)) AND ([ins].[ObjectID] = [insRole].[InsightId]))) OR ((([ins].[OrganisationOwnerId] = @__sharedOrgId_2) AND EXISTS (
//                          SELECT 1
//                          FROM [OrganisationInsights] AS [_]
//                          WHERE (([_].[OrganisationId] = @__currentUser_OrganisationId_0) AND ([_].[InsightId] = [ins].[ObjectID])) AND ([ins].[ObjectID] = [_].[InsightId]))) AND EXISTS (
//                          SELECT 1
//                          FROM [InsightRoles] AS [insRole0]
//                          INNER JOIN [BA_Role] AS [insRole.RoleO0] ON [insRole0].[RoleOID] = [insRole.RoleO0].[ObjectID]
//                          WHERE (EXISTS (
//                              SELECT 1
//                              FROM [BA_UserRole] AS [usrRole0]
//                              WHERE ([usrRole0].[UserId] = @__currentUser_ObjectId_1) AND ([insRole.RoleO0].[ObjectID] = [usrRole0].[RoleOID])) AND ([insRole.RoleO0].[OrganisationId] = @__currentUser_OrganisationId_0)) AND ([ins].[ObjectID] = [insRole0].[InsightId]))) */

//                    predicate = ins => ins.Active && //insight is active
//                                    ((ins.OrganisationOwnerId == user.OrganisationId //insight is in user org
//                                        && ins.InsightRoles //AND user is in a role that contains the insight
//                                                    .Any(insRole => insRole.RoleO.BaUserRoles.Any(
//                                                            usrRole => usrRole.UserId == user.ObjectId)
//                                                            && insRole.RoleO.OrganisationId == user.OrganisationId)
//                                       )
//                                       || (ins.OrganisationOwnerId == sharedOrgId // insight is in shared org
//                                           && ins.OrganisationInsights //AND insight has been provisioned to user org
//                                                    .Any(_ => _.OrganisationId == user.OrganisationId
//                                                            && _.InsightId == ins.ObjectId)
//                                           && ins.InsightRoles //AND user is in a role that contains the insight
//                                                    .Any(insRole => insRole.RoleO.BaUserRoles.Any(
//                                                            usrRole => usrRole.UserId == user.ObjectId)
//                                                            && insRole.RoleO.OrganisationId == user.OrganisationId)));
//                }
//            }

//            if (predicate == null)
//            {
//                throw new UnauthorizedException($"User {user.Name} does not have a valid role" +
//                    $"to access insight with action {configuration.ActionType}");
//            }

//            return predicate;
//        }
//#pragma warning restore 1998
//    }
//}

using covnet.AAA.Contracts;
using covnet.Models.EfCoreModels;
using covnet.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;

namespace covnet.AAA
{
    public class ClaimsVerification : IClaimsVerfication
    {
        #region Constants

        public const string CLAIM_ISSUER = "Analytics";
        public const string CLAIM_TYPE_USER_ID = "userId";
        public const string CLAIM_TYPE_EMAIL = "email";
        public const string CLAIM_TYPE_ACID = "acid";
        public const string CLAIM_TYPE_SCOPE = "scope";
        public const string CLAIM_TYPE_USER_DETAILS = "userDetails";
        public const string CLAIM_TYPE_SHARED_ORGANISATION_ID = "sharedOrgId";
        public const string CLAIM_VALUE_ACCESS_CLOUD_API = "access.cloud.analytics.api";
        public const string CLAIM_VALUE_ACCESS_WIDGET_API = "access.widget.api";
        public const string CLAIM_VALUE_ACCESS_WIDGET_DISCOVERY = "access.widget.discovery";

        #endregion

        #region Fields

        private readonly ClaimsPrincipal _user;

        #endregion

        #region Construction


        public ClaimsVerification(ClaimsPrincipal user)
        {
            _user = user;
        }

        #endregion

        #region Methods

        public Guid GetUserGuid()
        {
            if(_user == null)
            {
                return Guid.Empty;
            }

            Claim claim = _user?.Claims?.FirstOrDefault(cl => cl.Type == CLAIM_TYPE_ACID);
            if (!Guid.TryParse(claim?.Value, out var acidGuid))
            {
                throw new UnauthorizedException($"Could not get a valid Guid from {claim?.Value}");
            }
            return acidGuid;
        }

        public bool CanAccessAPI()
        {
            if (_user == null) {
                return false;
            }

            bool result = false;

            List<Claim> claims = _user.Claims?.Where(cl => cl.Type == CLAIM_TYPE_SCOPE).ToList();

            claims?.ForEach(claim => 
            {
                if (!string.IsNullOrEmpty(claim.Value))
                {   

                    if (claim.Value == CLAIM_VALUE_ACCESS_CLOUD_API) {
                        result = true;
                    }

                    if(claim.Value == CLAIM_VALUE_ACCESS_WIDGET_API) {
                        result = true;
                    }

                    if (claim.Value == CLAIM_VALUE_ACCESS_WIDGET_DISCOVERY) {
                        result = true;
                    }
                }
            });

            return result;
        }

        public int GetAnalyticsUserId()
        {
            return int.Parse(_user?.Claims?.FirstOrDefault(claim => claim.Type == CLAIM_TYPE_USER_ID)?.Value);
        }

        public User GetAnalyticsUser()
        {
            string jsonUser = _user?.Claims?.FirstOrDefault(claim => claim.Type == ClaimsVerification.CLAIM_TYPE_USER_DETAILS)?.Value;
            return (jsonUser != null) ? Newtonsoft.Json.JsonConvert.DeserializeObject<User>(jsonUser) : null;
        }

        #endregion
    }
}

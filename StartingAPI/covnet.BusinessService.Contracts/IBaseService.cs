using covnet.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace covnet.BusinessServices.Contracts
{
    public interface IBaseService<T> where T : class
    {
        Task<T> GetByPKAsync(
            int pk);

        Task<IEnumerable<T>> GetByPKManyAsync(
            IEnumerable<int> pks);

        Task<bool> DeleteAsync(
            T entity);

        Task<bool> DeleteAsync(
            IEnumerable<T> entities);

        Task<IEnumerable<T>> GetAllAsync();

        Task<IEnumerable<T>> GetAllAsync(
            Expression<Func<T, bool>> predicate);

        Task<IEnumerable<T>> GetAllWithLinkedEntitiesAsync();

        Task<IEnumerable<T>> GetAllWithNamedEntitiesAsync(
            string[] navigationProperties);

        Task<T> GetByIdWithNamedEntitiesAsync(
            int id,
            string[] navigationProperties);        

        Task<T> UpdateAsync(
            T entity);

        Task<IEnumerable<T>> UpdateAsync(
            IEnumerable<T> entities);

        Task<T> InsertAsync(
            T entity);

        Task<IEnumerable<T>> InsertAsync(
            IEnumerable<T> entities);
    }
}

﻿using covnet.Models.Base;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace covnet.BusinessServices.Contracts
{
    public interface IBaseServiceForBaseObjects<T> : IBaseService<T>
        where T : class, IBaseEntityModel
    {
        Task<IEnumerable<T>> GetAllAccessibleAsync();
    }
}

﻿using covnet.BusinessServices.Contracts;
using covnet.Models.EfCoreModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace covnet.BusinessService.Contracts
{
    public interface IUserService : IBaseService<User>
    {

    }
}

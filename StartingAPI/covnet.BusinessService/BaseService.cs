﻿using covnet.BusinessServices.Contracts;
using covnet.Models;
using covnet.Repository.Contracts;
using covnet.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace covnet.BusinessServices
{
    public class BaseService<T> : IBaseService<T> where T : class
    {
        #region Fields

        protected readonly ICrudRepository<T> Repository;

        #endregion

        #region Construction

        public BaseService(ICrudRepository<T> repository)
        {
            Repository = repository;
        }

        #endregion

        #region Methods

        public async Task<T> InsertAsync(
            T entity)
            //=> Repository.ExecuteInsertAsync(entity);
            => (await InsertAsync(Utils.SingletonList(entity))).FirstOrDefault();

        public virtual Task<IEnumerable<T>> InsertAsync(
            IEnumerable<T> entities)
            =>  Repository.ExecuteInsertAsync(entities);        

        public async Task<T> UpdateAsync(
            T entity)
            //=>  Repository.ExecuteUpdateAsync(entity);
            => (await UpdateAsync(Utils.SingletonList(entity))).FirstOrDefault();

        public virtual Task<IEnumerable<T>> UpdateAsync(
            IEnumerable<T> entities)
            => Repository.ExecuteUpdateAsync(entities);

        public Task<bool> DeleteAsync(
            T entity)
             //=> Repository.ExecuteDeleteAsync(entity);
             => DeleteAsync(Utils.SingletonList(entity));

        public virtual Task<bool> DeleteAsync(
            IEnumerable<T> entities)
            => Repository.ExecuteDeleteAsync(entities);

        public virtual Task<IEnumerable<T>> GetAllAsync()
            => Repository.GetAllAsync();

        public Task<IEnumerable<T>> GetAllAsync(
            Expression<Func<T, bool>> predicate)
            => Repository.GetAllAsync(predicate);

        public Task<IEnumerable<T>> GetAllWithLinkedEntitiesAsync()
            => Repository.GetAllWithLinkedEntitiesAsync();

        public Task<IEnumerable<T>> GetAllWithNamedEntitiesAsync(
            string[] navigationProperties)
            => Repository.GetAllWithNamedEntitiesAsync(navigationProperties);

        public Task<T> GetByIdWithNamedEntitiesAsync(
            int id, 
            string[] navigationProperties)
            => Repository.GetByIdWithNamedEntitiesAsync(
            id, 
            navigationProperties);

        public async Task<T> GetByPKAsync(int pk) 
        {
            T result = await Repository.GetByIdAsync(pk);

            if (result == default(T))
            {
                throw new NotFoundException($"No resource exists with id: {pk}");
            }

            return result;
        }

        public async Task<IEnumerable<T>> GetByPKManyAsync(IEnumerable<int> pks)
        {
            if (Utils.IsNullOrEmpty(pks))
            {
                return Enumerable.Empty<T>();
            }

            IList<T> result = new List<T>();

            foreach (int pk in pks.ToHashSet())
            {
                T resource = await Repository.GetByIdAsync(pk);
                if (resource != default(T))
                {
                    result.Add(resource);
                }
            }

            return result;
        }

        #endregion
    }
}

﻿using covnet.AAA.Contracts;
using covnet.BusinessServices.Contracts;
using covnet.Models.Base;
using covnet.Repository.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace covnet.BusinessServices
{
    public class BaseServiceForBaseObjects<T> : BaseService<T>, IBaseServiceForBaseObjects<T>
        where T : class, IBaseEntityModel
    {
        #region Fields

        private readonly IAuthenticationAndAuthorization _aaa;

        #endregion

        #region Construction

        public BaseServiceForBaseObjects(
            IAuthenticationAndAuthorization aaa,
            ICrudRepository<T> repository)
            : base(repository)
        {
            _aaa = aaa;
        }

        #endregion

        #region Override Methods

        public override Task<IEnumerable<T>> InsertAsync(IEnumerable<T> entities)
            => base.InsertAsync(entities.Select(_ => BeforeInsert(_)).ToList());


        public override Task<IEnumerable<T>> UpdateAsync(IEnumerable<T> entities)
            => base.UpdateAsync(entities.Select(_ => BeforeUpdate(_)).ToList());
        
        #endregion

        #region Own Methods

        public virtual async Task<IEnumerable<T>> GetAllAccessibleAsync()
        {
            IEnumerable<T> resources = await GetAllAsync();
            var accessibleResources = new List<T>();
            
            AuthorizationConfiguration configuration = new AuthorizationConfiguration
            {
                ActionType = ActionType.View
            };

            foreach (T r in resources)
            {
                if (await _aaa.IsCurrentUserAuthorizedAsync(r,configuration))
                {
                    accessibleResources.Add(r);
                }
            }
            return accessibleResources;
        }

        #endregion

        #region Private and Protected Methods

        protected IAuthenticationAndAuthorization GetAaa()
        {
            return _aaa;
        }

        private T BeforeInsert(T entity)
        {
            var now = DateTime.Now;
            entity.CrDate = now;

            entity.ModDate = now;

            return entity;
        }


        protected T BeforeUpdate(T entity)
        // HACK: This is made protected to be accessible from DataSetService where Update is overriden by a stored procedure
        {
            entity.ModDate = DateTime.Now;

            return entity;
        }

        #endregion
    }
}

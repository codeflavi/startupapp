﻿using covnet.AAA.Contracts;
using covnet.BusinessService.Contracts;
using covnet.BusinessServices;
using covnet.Models.EfCoreModels;
using covnet.Repository.Contracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace covnet.BusinessService
{
    public class UserService : BaseService<User>, IUserService
    {
        private readonly ITransactionManager Tx;
        private readonly IUserRepository _userRepository;
        public UserService(
            IAuthenticationAndAuthorization aaa,
            IUserRepository usersRepo,
            ILogger<UserService> logger,
            ITransactionManager tx)
            : base(usersRepo)
        {
            _userRepository = usersRepo;
            Tx = tx;
        }
    }
}

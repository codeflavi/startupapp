﻿using covnet.Models.EfCoreModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace covnet.Models.EFCoreModels
{

    public partial class CovnetContext : CovidContext
    {

        private readonly IConfiguration _configuration;


        public static readonly ILoggerFactory _contextDebugLogger
            = LoggerFactory.Create(builder =>
        {
            builder
            .AddFilter((category, level) => 
                category == DbLoggerCategory.Database.Command.Name &&
                level == LogLevel.Information)
            .AddConsole();
        });
        

        public CovnetContext(IConfiguration configuration) {
            _configuration = configuration;           
        }

        public CovnetContext(DbContextOptions<CovidContext> options)
            : base(options)
        {
            
        }

        protected override void OnConfiguring(
            DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            if (!optionsBuilder.IsConfigured) {

                optionsBuilder.UseSqlServer(_configuration["ConnectionStrings:CovidConnection"]);

#if DEBUG
                optionsBuilder.EnableSensitiveDataLogging(true);
                optionsBuilder.EnableDetailedErrors(true);
                optionsBuilder.UseLoggerFactory(_contextDebugLogger);
#endif               
            }
        }

    }
}

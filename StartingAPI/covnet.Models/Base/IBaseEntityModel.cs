﻿using System;
using System.ComponentModel.DataAnnotations;

namespace covnet.Models.Base
{
    public interface IBaseEntityModel : IWithObjectId
    {

        [Required]
        DateTime CrDate { get; set; }

        [Required]
        string CrUser { get; set; }

        [Required]
        DateTime ModDate { get; set; }

        [Required]
        string ModUser { get; set; }
    }
}

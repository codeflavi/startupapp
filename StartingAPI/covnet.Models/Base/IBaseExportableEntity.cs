﻿using System;
using System.ComponentModel.DataAnnotations;

namespace covnet.Models.Base
{
    public interface IBaseExportableEntity
    {
        [Required]        
        Guid ExternalId { get; set; }

    }
}

﻿using System;

namespace covnet.Models.Base
{
    public interface IBaseMetadata : ISimplifiedBaseMetadata
    {

        #region Properties

        string CrUser { get; set; }

        DateTime CrDate { get; set; }

        string ModUser { get; set; }

        DateTime ModDate { get; set; }

        bool? Shared { get; set; }

        bool CanEdit { get; set; }

        bool CanDelete { get; set; }

        #endregion

    }
}

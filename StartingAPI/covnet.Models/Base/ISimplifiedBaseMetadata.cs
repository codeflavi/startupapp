﻿using System;

namespace covnet.Models.Base
{
    public interface ISimplifiedBaseMetadata
    {

        #region Properties

        int ObjectId { get; set; }

        string Name { get; set; }

        string Description { get; set; }

        int OrganisationId { get; set; }

        #endregion

    }
}

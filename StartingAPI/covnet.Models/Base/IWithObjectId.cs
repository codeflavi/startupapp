﻿using System.ComponentModel.DataAnnotations;

namespace covnet.Models.Base
{
    public interface IWithObjectId
    {
        [Required]
        int ObjectId { get; set; }

    }
}

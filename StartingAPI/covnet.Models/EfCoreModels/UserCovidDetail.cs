﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace covnet.Models.EfCoreModels
{
    public partial class UserCovidDetail
    {
        public int Id { get; set; }
        [Key]
        public int UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("UserCovidDetail")]
        public virtual User User { get; set; }
    }
}
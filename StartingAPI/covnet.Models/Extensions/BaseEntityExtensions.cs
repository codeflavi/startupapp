﻿using covnet.Models.Base;

namespace covnet.Models.Extensions
{
    public static class BaseEntityExtensions
    {
        public static bool IsNew(this IWithObjectId entity)
            => entity.ObjectId == 0;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace covnet.Models.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> NonNull<T>(
            this IEnumerable<T> source) 
            => source.Where(_ => _ != null);

        public static IEnumerable<TResult> SelectNonNull<TSource, TResult>(
            this IEnumerable<TSource> source, 
            Func<TSource, TResult> selector)
            => source.NonNull().Select(selector).NonNull();     
    }
}

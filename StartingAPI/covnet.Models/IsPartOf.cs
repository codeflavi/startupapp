﻿using System;

namespace covnet.Models
{
    [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public class IsPartOfAttribute : Attribute
    {
    }
}

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace covnet.Models
{
    public static class Utils
    {
        public static ICollection<T> SingletonList<T>(T item)
        {
            return item != null
                ? new System.Collections.ObjectModel.ReadOnlyCollection<T>(new T[] { item })
                : new System.Collections.ObjectModel.ReadOnlyCollection<T>(new T[] { });
        }

        public static bool IsNullOrEmpty<T>(IEnumerable<T> col)
        {
            return !col?.Any() ?? true;
        }

        public static IEnumerable<T> OrEmptyIfNull<T>(IEnumerable<T> source)
        {
            return source ?? Enumerable.Empty<T>();
        }

        public static IEnumerable<R> SelectAsync<T, R>(this IEnumerable<T> source, Func<T, Task<R>> selector)
        {
            return source
                .Select(selector)
                .Select(_ => _.Result)
                .Where(_ => _ != null);
        }

        public static IEnumerable<T> DistinctBy<T, K>
            (this IEnumerable<T> source,
            Func<T, K> keySelector)
            => source?.GroupBy(keySelector).Select(_ => _.First());

        public static bool IsNullOrEmpty(this JToken token)
        {
            return (token == null) ||
                   (token.Type == JTokenType.Array && !token.HasValues) ||
                   (token.Type == JTokenType.Object && !token.HasValues) ||
                   (token.Type == JTokenType.String && token.ToString() == String.Empty) ||
                   (token.Type == JTokenType.Null);
        }

        public static IEnumerable<int> ParseJSON(
            string json,
            string jsonPath)
        {
            IEnumerable<int> ids = string.IsNullOrEmpty(json) ?
                Enumerable.Empty<int>() :
                JsonConvert.DeserializeObject<JContainer>(json)
                    .SelectMany(_ => _.SelectTokens(jsonPath))
                    .Where(_ => !_.IsNullOrEmpty())
                    .Select(_ => _.Value<int>())
                    .Distinct()
                    .ToList();

            return ids;
        }

        public static IEnumerable<int> ParseJSONByReportId(
            string json)
            => ParseJSON(json, "$..reportId");
    }
}

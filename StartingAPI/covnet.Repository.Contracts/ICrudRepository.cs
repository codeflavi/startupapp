﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace covnet.Repository.Contracts
{
    public interface ICrudRepository<T> where T : class
    {
        #region Methods

        Task<T> ExecuteInsertAsync(
            T entity);

        Task<IEnumerable<T>> ExecuteInsertAsync(
            IEnumerable<T> entities);

        Task<T> ExecuteUpdateAsync(
            T entity);

        Task<IEnumerable<T>> ExecuteUpdateAsync(
            IEnumerable<T> entities);

        Task<bool> ExecuteDeleteAsync(
            T entity);

        Task<bool> ExecuteDeleteAsync(
            IEnumerable<T> entities);

        Task<IEnumerable<T>> GetAllAsync();

        Task<IEnumerable<T>> GetAllWithLinkedEntitiesAsync();
        
        Task<IEnumerable<T>> GetAllWithNamedEntitiesAsync(
            string[] navigationPaths);

        Task<IEnumerable<T>> GetAllWithNamedEntitiesAsync(
            string[] navigationProperties,
            Expression<Func<T, bool>> predicate);

        Task<T> GetByIdWithNamedEntitiesAsync(
            int id,
            string[] navigationPaths);
        

        Task<IEnumerable<T>> GetAllAsync(
            Expression<Func<T, bool>> predicate);

        Task<T> GetAsync(
            Expression<Func<T, bool>> predicate);

        Task<T> GetByIdAsync(
            int id);

        Task<T> GetAsync(
           Expression<Func<T, bool>> predicate,
           string includedEntityName);

        Task<T> EnsureAuthorizationDataAsync(
            T entity);

        Task<IEnumerable<T>> ReplaceAsync(
            IEnumerable<T> newEntities,
            Expression<Func<T, bool>> deletePredicate);

        #endregion
    }
}

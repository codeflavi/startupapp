﻿using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace covnet.Repository.Contracts
{
    public interface IPrimaryKeyRetriever
    {
        int GetPrimaryKeyValue<T>(
            IModel model,
            T entity);


        string GetPrimaryKeyName<T>(
            IModel model,
            T entity);

    }
}

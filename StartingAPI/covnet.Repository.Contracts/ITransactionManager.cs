﻿using System;
using System.Threading.Tasks;

namespace covnet.Repository.Contracts
{
    public interface ITransactionManager
    {
        Task<T> ExecuteWithinTransactionAsync<T>(
            Func<Task<T>> producer);
    }
}

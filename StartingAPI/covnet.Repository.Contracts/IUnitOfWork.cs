﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Data;

namespace covnet.Repository.Contracts
{
    // This is the public interface of IUnitOfWork used in other projects
    //
    public interface IUnitOfWork : IDisposable
    {
    //    IDbConnection Connection { get; }

    //    IDbTransaction Transaction { get; }


        IDbContextTransaction Transaction { get; }

        DbContext EFContext { get; }

    }    
}

﻿using covnet.Models.EfCoreModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace covnet.Repository.Contracts
{
    public interface IUserRepository : ICrudRepository<User>
    {
        
    }
}

﻿using covnet.Models;
using covnet.Models.Base;
using covnet.Models.Extensions;
using covnet.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace covnet.Repository
{
    public class CrudRepository<T> : DatabaseCommons, ICrudRepository<T> where T : class
    {
        #region Construction

        public CrudRepository(
            ITransactionManagerInternal tx,
            ILogger<CrudRepository<T>> logger)
            : base(tx, logger)
        {
        }

        #endregion

        #region Methods

        public virtual Task<T> ExecuteInsertAsync(
            T entity)
        {
            if (entity == default(T))
            {
                return Task.FromResult(entity);
            }

            return Tx.ExecuteWithinTransactionAsync(/*async*/ (unitOfWork) =>
            {
                return Execute(unitOfWork, entity, async () =>
                {
                    unitOfWork.EFContext.Add(entity);
                    await unitOfWork.EFContext.SaveChangesAsync();
                });
            });
        }

        public virtual Task<IEnumerable<T>> ExecuteInsertAsync(
           IEnumerable<T> entities)
        {
            if (Utils.IsNullOrEmpty(entities))
            {
                return Task.FromResult(entities as IEnumerable<T>);
            }

            return Tx.ExecuteWithinTransactionAsync(/*async*/ (unitOfWork) =>
            {
                return Execute(unitOfWork, entities, async () =>
                {
                    unitOfWork.EFContext.AddRange(entities);
                    await unitOfWork.EFContext.SaveChangesAsync();
                });
                //return (IEnumerable<T>)entities;
            });
        }

        public virtual Task<T> ExecuteUpdateAsync(
            T entity)
        {
            if (entity == default(T))
            {
                return Task.FromResult(entity);
            }

            return Tx.ExecuteWithinTransactionAsync(/*async*/ (unitOfWork) =>
            {
                return Execute(unitOfWork, entity, async () =>
                {
                    unitOfWork.EFContext.Update(entity);
                    await unitOfWork.EFContext.SaveChangesAsync();
                });
                //return entity;
            });
        }

        public virtual Task<IEnumerable<T>> ExecuteUpdateAsync(
            IEnumerable<T> entities)
        {
            if (Utils.IsNullOrEmpty(entities))
            {
                return Task.FromResult(entities as IEnumerable<T>);
            }

            return Tx.ExecuteWithinTransactionAsync(/*async*/ (unitOfWork) =>
            {
                return Execute(unitOfWork, entities, async () =>
                {
                    unitOfWork.EFContext.UpdateRange(entities);
                    await unitOfWork.EFContext.SaveChangesAsync();
                });
                //return (IEnumerable<T>)entities;
            });
        }

        public virtual Task<bool> ExecuteDeleteAsync(
          T entity)
        {
            if (entity == default(T))
            {
                return Task.FromResult(true);
            }

            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                await Execute(unitOfWork, entity, async () =>
                {
                    unitOfWork.EFContext.Remove(entity);
                    await unitOfWork.EFContext.SaveChangesAsync();
                });
                return true;
            });
        }

        public virtual Task<bool> ExecuteDeleteAsync(
          IEnumerable<T> entities)
        {
            if (Utils.IsNullOrEmpty(entities))
            {
                return Task.FromResult(true);
            }

            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                await Execute(unitOfWork, entities, async () =>
                {
                    unitOfWork.EFContext.RemoveRange(entities);
                    await unitOfWork.EFContext.SaveChangesAsync();
                });
                return true;
            });
        }

        public virtual Task<IEnumerable<T>> GetAllAsync()
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                return await Task.FromResult(
                    unitOfWork
                    .EFContext
                    .Set<T>()
                    .AsNoTracking()
                    .ToList()
                    .AsEnumerable());
            });
        }

        public virtual Task<IEnumerable<T>> GetAllAsync(
            Expression<Func<T, bool>> predicate)
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                return await Task.FromResult(
                    unitOfWork
                    .EFContext
                    .Set<T>()
                    .Where(predicate)
                    .AsNoTracking()
                    .ToList()
                    .AsEnumerable());
            });
        }

        public virtual Task<T> GetByIdAsync(
            int id)
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                //return await unitOfWork.EFContext.FindAsync<T>(id);

                //changed from above, so that the item is not tracked
                var entity = await unitOfWork.EFContext.FindAsync<T>(id);

                if (entity != null &&
                        unitOfWork.EFContext.Entry(entity) != null)
                {
                    unitOfWork.EFContext.Entry(entity).State = EntityState.Detached;
                }

                return entity;
            });
        }

        public virtual Task<T> GetAsync(
            Expression<Func<T, bool>> predicate)
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                return await Task.FromResult(
                    unitOfWork
                    .EFContext
                    .Set<T>()
                    .Where(predicate)
                    .AsNoTracking()
                    .FirstOrDefault());

            });
        }

        public virtual Task<T> GetAsync(
            Expression<Func<T, bool>> predicate,
            string includedEntityName)
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                return await Task.FromResult(
                    unitOfWork
                    .EFContext
                    .Set<T>()
                    .Where(predicate)
                    .Include(includedEntityName)
                    .AsNoTracking()
                    .FirstOrDefault());
            });
        }

        public virtual Task<IEnumerable<T>> GetAllWithNamedEntitiesAsync(
            string[] navigationProperties)
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                await ValidateNavigationAsync(
                    unitOfWork,
                    navigationProperties);

                return await Task.FromResult(
                    unitOfWork
                    .EFContext
                    .Set<T>()
                    .IncludeMultiple(navigationProperties.ToArray())
                    .AsNoTracking()
                    .AsEnumerable());
            });
        }

        public virtual Task<IEnumerable<T>> GetAllWithNamedEntitiesAsync(
            string[] navigationProperties,
            Expression<Func<T, bool>> predicate)
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                await ValidateNavigationAsync(
                    unitOfWork,
                    navigationProperties);

                return await Task.FromResult(
                    unitOfWork
                    .EFContext
                    .Set<T>()
                    .Where(predicate)
                    .IncludeMultiple(navigationProperties.ToArray())
                    .AsNoTracking()
                    .AsEnumerable());
            });
        }

        public virtual Task<T> GetByIdWithNamedEntitiesAsync(
            int id,
            string[] navigationProperties)
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                await ValidateNavigationAsync(
                    unitOfWork,
                    navigationProperties);

                return await Task.FromResult(
                    unitOfWork
                    .EFContext
                    .Set<T>()
                    .IncludeMultiple(navigationProperties.ToArray())
                    .AsNoTracking()
                    .Where(d => ((IBaseEntityModel)d).ObjectId == id)
                    .FirstOrDefault());

            });
        }

        public virtual Task<IEnumerable<T>> GetAllWithLinkedEntitiesAsync()
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                return await Task.FromResult(
                    unitOfWork
                    .EFContext
                    .Set<T>()
                    .Include(unitOfWork.EFContext.GetIncludePaths(typeof(T)))
                    .AsNoTracking()
                    .AsEnumerable());
            });
        }

        public virtual Task<T> EnsureAuthorizationDataAsync(
            T entity)
            => Task.FromResult(entity);

        public async Task<IEnumerable<T>> ReplaceAsync(
            IEnumerable<T> newEntities,
            Expression<Func<T, bool>> deletePredicate)
        {
            // Delete existing relationsips
            IEnumerable<T> filters = (deletePredicate != default) ? await GetAllAsync(deletePredicate) : Enumerable.Empty<T>();

            await ExecuteDeleteAsync(filters);

            // The approach above will query first the database to retrieve the list of ids that need to be deleted 
            // followed by N subsequent DELETE commands, one for each id
            // If ths proved to be a performance bottleneck, then uncomment to line below and delete everything in 
            // one DELETE ... WHERE ... call
            //
            //await unitOfWork.EFContext.Database.ExecuteSqlCommandAsync("DELETE {TableName} WHERE {Predicate}", new object[] { entity.ObjectId });

            // Add new ones
            return (newEntities?.Any() == true) ?
                await ExecuteInsertAsync(newEntities) :
                newEntities;
        }

        internal Task ValidateNavigationAsync(
            IUnitOfWork work,
            string[] navigationProperties)
        {

            var entityType = work.EFContext.Model.FindEntityType(typeof(T));
            var navigations = entityType.GetNavigations();

            navigationProperties.ToList().ForEach(np => {

                var match = navigations
                    .ToList()
                    .Where(n => n.Name == np)
                    .FirstOrDefault();

                if (match == null)
                {
                    throw new Exception($"{np} is not a navigation property of {nameof(T)}");
                }
            });

            return Task.FromResult(true);

        }

        private async Task<T> Execute(
            IUnitOfWork unitOfWork,
            T entity,
            Func<Task> producer)
            => (await Execute(unitOfWork, Utils.SingletonList(entity), producer)).FirstOrDefault();

        //private async Task<T> Execute(
        //    IUnitOfWork unitOfWork,
        //    T entity,
        //    Func<Task> producer)
        //{
        //    IEnumerable<PropertyInfo> foreignKeys = typeof(T)
        //        .GetProperties()
        //        .Where(property => 
        //            property.GetCustomAttributes(typeof(ForeignKeyAttribute), true).Any()
        //            || property.GetCustomAttributes(typeof(InversePropertyAttribute), true).Any())
        //        .ToList();

        //    Dictionary<PropertyInfo, dynamic> memento = foreignKeys
        //        .ToDictionary(_ => _,
        //                      _ => _.GetValue(entity));

        //    foreach (PropertyInfo fk in foreignKeys)
        //    {
        //        fk.SetValue(entity, null);
        //    }

        //    try
        //    {
        //        await producer();

        //        // Prevent EF from persisting the entity in the Context
        //        // unitOfWork.EFContext.Entry(entity).State = EntityState.Detached;
        //        if (unitOfWork != default(IUnitOfWork))
        //        {
        //            var cachedEntity = unitOfWork.EFContext.Entry(entity);
        //            if (cachedEntity != default(T))
        //            {
        //                cachedEntity.State = EntityState.Detached;
        //            }
        //        }
        //    }
        //    finally
        //    {
        //        foreach (PropertyInfo fk in foreignKeys)
        //        {
        //            fk.SetValue(entity, memento[fk]);
        //        }
        //    }

        //    return entity;
        //}

        private async Task<IEnumerable<T>> Execute(
            IUnitOfWork unitOfWork,
            IEnumerable<T> entities,
            Func<Task> producer)
        {
            IEnumerable<PropertyInfo> foreignKeys = typeof(T)
                .GetProperties()
                .Where(property =>
                    property.GetCustomAttributes(typeof(ForeignKeyAttribute), true).Any()
                    || (property.GetCustomAttributes(typeof(InversePropertyAttribute), true).Any()
                        && !property.GetCustomAttributes(typeof(IsPartOfAttribute), true).Any()))
                .ToList();

            Dictionary<T, Dictionary<PropertyInfo, dynamic>> memento = entities
                .ToDictionary(
                    entity => entity,
                    entity => foreignKeys
                        .ToDictionary(
                            fk => fk,
                            fk => fk.GetValue(entity)));

            foreach (T entity in entities)
            {
                foreach (PropertyInfo fk in foreignKeys)
                {
                    fk.SetValue(entity, null);
                }
            }

            try
            {
                await producer();
            }
            finally
            {
                foreach (T entity in entities)
                {
                    // Prevent EF from persisting the entity in the Context
                    if (unitOfWork != default(IUnitOfWork))
                    {
                        var cachedEntity = unitOfWork.EFContext.Entry(entity);
                        if (cachedEntity != default(T))
                        {
                            cachedEntity.State = EntityState.Detached;
                        }

                        // Remove the parts of the aggregate from EF Context as well
                        IEnumerable<PropertyInfo> partOfAttribtues = typeof(T)
                            .GetProperties()
                            .Where(property => property.GetCustomAttributes(typeof(IsPartOfAttribute), true).Any())
                            .ToList();

                        foreach (PropertyInfo partOf in partOfAttribtues)
                        {
                            var part = partOf.GetValue(entity);

                            if (part != default)
                            {
                                if (part is IEnumerable children)
                                {
                                    foreach (var p in children)
                                    {
                                        var cachedPartEntity = unitOfWork.EFContext.Entry(p);
                                        if (cachedPartEntity != default(T))
                                        {
                                            cachedPartEntity.State = EntityState.Detached;
                                        }
                                    }
                                }
                                else
                                {
                                    var cachedPartEntity = unitOfWork.EFContext.Entry(part);
                                    if (cachedPartEntity != default(T))
                                    {
                                        cachedPartEntity.State = EntityState.Detached;
                                    }
                                }
                            }
                        }
                    }

                    foreach (PropertyInfo fk in foreignKeys)
                    {
                        fk.SetValue(entity, memento[entity][fk]);
                    }
                }
            }

            return entities;
        }
        #endregion
    }
}
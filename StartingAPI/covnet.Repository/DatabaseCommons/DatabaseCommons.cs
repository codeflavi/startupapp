﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace covnet.Repository
{
    public class DatabaseCommons
    {
        #region Fields
        protected /*private*/ readonly ILogger Logger;

        protected /*private*/ readonly ITransactionManagerInternal Tx;

        #endregion

        #region Construction

        public DatabaseCommons(
            ITransactionManagerInternal tx,
            ILogger logger)
        {
            Logger = logger;
            Tx = tx;
        }

        #endregion

        #region Methods

        protected Task<T> ExecuteStoredProcedureQuerySingleAsync<T>(
           string storedProcedureName,
           List<SqlParameter> parameters) where T : class
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                try
                {
                    return await Task.FromResult(
                       unitOfWork
                       .EFContext
                       .Set<T>()
                       .FromSqlRaw(GetExecuteStatement(
                           storedProcedureName,
                           parameters), parameters.ToArray())
                       .AsEnumerable()
                       .FirstOrDefault());
                }
                catch (SqlException sqlException)
                {
                    Logger?.LogCritical(
                        sqlException,
                        "Unable to execute {}: {}",
                        storedProcedureName,
                        sqlException.Message);
                    throw;
                }
            });
        }

        protected async Task<IEnumerable<T>> ExecuteStoredProcedureQueryAsync<T>(
            string storedProcedureName,
            List<SqlParameter> parameters) where T : class
        {
            return await Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                try
                {
                    return await Task.FromResult(
                       unitOfWork
                       .EFContext
                       .Set<T>()
                       .FromSqlRaw(GetExecuteStatement(
                           storedProcedureName,
                           parameters), parameters.ToArray())
                       .ToList()
                       .AsEnumerable());
                }
                catch (SqlException sqlException)
                {
                    Logger?.LogCritical(
                        sqlException,
                        "Unable to execute {}: {}",
                        storedProcedureName,
                        sqlException.Message);
                    throw;

                }
            });
        }

        protected async Task<IEnumerable<T>> CallTVFAsync<T>(
            string tableValueFunctionName,
            List<SqlParameter> parameters) where T : class
        {
            return await Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                try
                {

                    return await Task.FromResult(
                      unitOfWork
                      .EFContext
                      .Set<T>()                       
                      .FromSqlRaw(GetSelectStatement(
                           tableValueFunctionName,
                           parameters), parameters.ToArray())
                      .AsNoTracking()
                      .ToList()                      
                      .AsEnumerable());
                }
                catch (SqlException sqlException)
                {
                    Logger?.LogCritical(
                        sqlException,
                        "Unable to execute {}: {}",
                        tableValueFunctionName,
                        sqlException.Message);
                    throw;
                }
                catch(Exception e)
                {
                    Logger?.LogCritical(
                        e,
                        "Unable to execute {}: {}",
                        tableValueFunctionName,
                        e.Message);
                    throw;
                }
            });
        }

        protected Task<int> ExecuteStoredProcedureAsync(
            string storedProcedureName,
            List<SqlParameter> parameters)
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                try
                {
                    return await unitOfWork
                       .EFContext
                       .Database
                       .ExecuteSqlRawAsync(
                            GetExecuteStatement(storedProcedureName, parameters), 
                            parameters.ToArray());
                }
                catch (SqlException sqlException)
                {
                    Logger?.LogCritical(
                        sqlException,
                        "Unable to execute {}: {}",
                        storedProcedureName,
                        sqlException.Message);
                    throw;
                }
            });
        }

        internal string GetExecuteStatement(
            string procedureName,
            List<SqlParameter> parameters)
        {
            StringBuilder sb = new StringBuilder();

            string parameterNames = string.Join(
                ",",
                parameters.Select(p => p.ParameterName));

            sb.Append($"{procedureName} {parameterNames}");

            return sb.ToString();

        }

        internal string GetSelectStatement(
            string tableName, 
            List<SqlParameter> parameters)
        {

            StringBuilder sb = new StringBuilder();

            string parameterNames = string.Join(
                ",",
                parameters.Select(p => p.ParameterName));

            sb.Append($"SELECT * FROM {tableName} ({parameterNames})");
            
            return sb.ToString();


        }

        #endregion
    }

}

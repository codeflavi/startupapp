﻿using covnet.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace covnet.Repository
{
    public class PrimaryKeyRetriever : IPrimaryKeyRetriever
    {
        public int GetPrimaryKeyValue<T>(
            IModel model,
            T entity)
        {
            string keyName = GetPrimaryKeyName(
                model, 
                entity);

            return (int)entity
                .GetType()
                .GetProperty(keyName)
                .GetValue(entity, null);
        }
        
        internal string GetPrimaryKeyName<T>(
            IModel model,
            T entity)
        {
            return model
                .FindEntityType(typeof(T))
                .FindPrimaryKey()
                .Properties
                .Select(x => x.Name)
                .Single();
        }

        string IPrimaryKeyRetriever.GetPrimaryKeyName<T>(
            IModel model, 
            T entity)
        {
            return GetPrimaryKeyName(
                model, 
                entity);
        }
    }
}

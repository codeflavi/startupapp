﻿using covnet.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace covnet.Repository
{
    public interface ITransactionManagerInternal : ITransactionManager
    {
        Task<T> ExecuteWithinTransactionAsync<T>(
            Func<IUnitOfWork, Task<T>> producer);
    }

    public sealed class TransactionManager : ITransactionManagerInternal, IDisposable
    {
        #region Fields

        private UnitOfWork UnitOfWork = default(UnitOfWork);

        private readonly ILogger<TransactionManager> Logger;                

        private readonly DbContext EFContext;

        #endregion

        #region Construction

        public TransactionManager(
            DbContext context,
            ILogger<TransactionManager> logger)
        {
            Logger = logger;            
            EFContext = context ?? throw new ArgumentNullException("Connection is null.");
        }

        #endregion

        #region Methods

        public Task<T> ExecuteWithinTransactionAsync<T>(
            Func<Task<T>> producer)
        {
            return ExecuteWithinTransactionAsync(async (_) => await producer());
        }

        public async Task<T> ExecuteWithinTransactionAsync<T>(
            Func<IUnitOfWork, Task<T>> producer)
        {
            T result = default(T);
                        
            UnitOfWork uowNew = new UnitOfWork(EFContext);


            //sets this.UnitOfWork = uowNew iff this.UnitOfWork==null (default(UnitOfWork))
            //otherwise, reuse the existing UnitOfWork object
            UnitOfWork uowOld = Interlocked.CompareExchange<UnitOfWork>(ref UnitOfWork, uowNew, default(UnitOfWork));
            UnitOfWork unitOfWork = uowOld != default(UnitOfWork) ? uowOld : uowNew;

            try
            {
                if (unitOfWork == uowNew)
                {                    
                    unitOfWork.Begin();
                }

                result = await producer(unitOfWork);

                //commit only in the call that created the unit of work
                if (unitOfWork == uowNew)
                {
                    unitOfWork.Commit();
                }
            }
            catch (Exception exception)
            {               
                Logger?.LogCritical(exception, "Exception while processing unitOfWork: {}", unitOfWork.Id);

                UnitOfWork?.Rollback();
                throw;
            }
            finally
            {
                if (unitOfWork == uowNew)
                {
                    CloseConnection();
                }
            }

            return result;
        }

        public void CloseConnection()
        {
            UnitOfWork = default(UnitOfWork);            
        }

        public void Dispose()
        {            
            EFContext?.Dispose();
        }

        #endregion
    }
}

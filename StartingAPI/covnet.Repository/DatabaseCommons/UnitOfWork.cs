﻿using covnet.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("covnet.UnitTests")]

namespace covnet.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Fields
                
        private readonly DbContext _context;
        public Guid Id { get; private set; }  = Guid.NewGuid();
        private IDbContextTransaction _contextTransaction = null;

        #endregion

        #region Construction

        internal UnitOfWork(
             DbContext context)
        {
            _context = context;
        }

        #endregion

        #region Properties

        DbContext IUnitOfWork.EFContext
        {
            get { return _context; }
        }

        IDbContextTransaction IUnitOfWork.Transaction
        {
            get { return _contextTransaction; }
        }       

        #endregion

        #region Methods
              
        internal IUnitOfWork Begin()
        {
            _contextTransaction = _contextTransaction ?? _context.Database.BeginTransaction();
            return this;
        }      

        internal void Commit()
        {
            _contextTransaction?.Commit();
            Dispose();
        }
       
        internal void Rollback()
        {
            _contextTransaction?.Rollback();
            Dispose();
        }

        public void Dispose()
        {
            _contextTransaction?.Dispose();
            _contextTransaction = null;
        }

        #endregion

    }
}

﻿using covnet.Models.EfCoreModels;
using covnet.Models.Extensions;
using covnet.Repository.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace covnet.Repository
{
    public class UserRepository : CrudRepository<User>, IUserRepository
    {
        #region Construction
        public UserRepository(
            ITransactionManagerInternal tx,
            ILogger<UserRepository> logger
            )
            : base(tx, logger)
        {
        }

        public override Task<IEnumerable<User>> GetAllAsync()
        {
            return Tx.ExecuteWithinTransactionAsync(async (unitOfWork) =>
            {
                return await Task.FromResult(
                    unitOfWork
                    .EFContext
                    .Set<User>()
                    .Include(_ => _.UsersDetail)
                    .AsNoTracking()
                    .ToList()
                    .AsEnumerable());
            });
        }
        #endregion
    }
}

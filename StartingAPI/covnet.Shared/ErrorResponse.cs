﻿using System;
using System.Collections.Generic;

namespace covnet.Shared
{
    public class ErrorResponse
    {
        #region Constants

        private static readonly string STATUS = "failure";
        
        #endregion

        #region Properties

        public string status { get; } = STATUS;

        public string message { get; }

        public IEnumerable<ErrorItem> errors { get; }

        public Guid identifier {get; } = Guid.NewGuid();

        public DateTime time { get; } = DateTime.Now;

        #endregion

        #region Constructors

        public ErrorResponse(string message, IEnumerable<ErrorItem> errors)
        {
            this.message = message;
            this.errors = errors;
        }
        
        #endregion
    }

    public class ErrorItem
    {
        #region Properties

        public string code { get; }

        public string message { get; }

        #endregion

        #region Constructors

        public ErrorItem(string code, string message)
        {
            this.code = code;
            this.message = message;
        }

        #endregion
    }
}

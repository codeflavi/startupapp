﻿using System;
using System.Net;

namespace covnet.Shared.Exceptions
{
    public class BadRequestException : BaseCustomException
    {
        #region Properties

        public override int StatusCode { get; } = (int)HttpStatusCode.BadRequest;
        private static string DefaultResponseMessage =
            "Request payload does not match the minimum expected state.";

        #endregion

        #region Construction

        public BadRequestException()
            : base(DefaultResponseMessage)
        {
                
        }

        public BadRequestException(string message)
            : base(message)
        {

        }

        public BadRequestException(string messgae, Exception innerException)
            : base(messgae, innerException)
        {

        }

        #endregion
    }
}

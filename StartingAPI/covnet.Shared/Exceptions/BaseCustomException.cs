﻿using System;
using System.Net;

namespace covnet.Shared.Exceptions
{
    public abstract class BaseCustomException : Exception
    {
        #region Properties

        public abstract int StatusCode { get; }

        public virtual string StatusMessage { get; } = "Unexpected error occurred";

        private const string DefaultResponseMessage =
            "A server-side error occurred while processing the request. Please check the server log files.";

        #endregion

        #region Construction

        public BaseCustomException()
            : base(DefaultResponseMessage)
        {
                
        }

        public BaseCustomException(string message)
            : base(message)
        {

        }

        public BaseCustomException(string messgae, Exception innerException)
            : base(messgae, innerException)
        {

        }

        #endregion
    }
}

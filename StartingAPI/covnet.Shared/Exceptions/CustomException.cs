﻿namespace covnet.Shared.Exceptions
{
    public enum CustomStatusCodes
    {
        INSIGHT_IN_USE = 5001,
        FILE_HAS_0_BYTES = 5002
    }

    public class CustomInternalServerException : InternalServerException
    {
        #region Properties

        public int CustomCode { get; set;  }

        #endregion

        #region Construction

        public CustomInternalServerException(int CustomCode)
            : this(CustomCode, "")
        {
        }

        public CustomInternalServerException(int CustomStatusCode, string message)
            : base(message)
        {
            CustomCode = CustomStatusCode;
        }

        #endregion
    }
}

﻿using System;
using System.Net;

namespace covnet.Shared.Exceptions
{
    public class EmptyFileException : BaseCustomException
    {
        #region Properties

        public override int StatusCode { get; } = (int)HttpStatusCode.BadRequest;
        private static string DefaultResponseMessage =
            "Request payload does not match the minimum expected state.";

        #endregion

        #region Construction

        public EmptyFileException()
            : base(DefaultResponseMessage)
        {
                
        }

        public EmptyFileException(string message)
            : base(message)
        {

        }

        public EmptyFileException(string messgae, Exception innerException)
            : base(messgae, innerException)
        {

        }

        #endregion
    }
}

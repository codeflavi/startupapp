﻿using System;
using System.Net;

namespace covnet.Shared.Exceptions
{
    public class ImportFileException : BaseCustomException
    {
        #region Properties

        public override int StatusCode { get; } = (int)HttpStatusCode.InternalServerError;

        public override string StatusMessage { get; } = "An exception occurred while processing the uploaded file";

        private static string DefaultResponseMessage =
            "Failed to process uploaded file.";

        #endregion

        #region Construction

        public ImportFileException()
            : base(DefaultResponseMessage)
        {

        }

        public ImportFileException(string message)
            : base(message)
        {

        }

        public ImportFileException(string messgae, Exception innerException)
            : base(messgae, innerException)
        {

        }

        #endregion
    }
}
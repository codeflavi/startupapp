﻿using System;
using System.Net;

namespace covnet.Shared.Exceptions
{
    public class InternalServerException : BaseCustomException
    {
        #region Properties

        public override int StatusCode { get; } = (int)HttpStatusCode.InternalServerError;

        #endregion

        #region Construction

        public InternalServerException()
            : base()
        {
                
        }

        public InternalServerException(string message)
            : base(message)
        {

        }

        public InternalServerException(string messgae, Exception innerException)
            : base(messgae, innerException)
        {

        }

        #endregion
    }
}

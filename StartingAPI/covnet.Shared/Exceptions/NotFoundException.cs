﻿using System;
using System.Net;

namespace covnet.Shared.Exceptions
{
    public class NotFoundException : BaseCustomException
    {
        #region Properties

        public override int StatusCode { get; } = (int)HttpStatusCode.NotFound;

        public override string StatusMessage { get; } = "The resource requested was not found";

        private static string DefaultResponseMessage =
            "Could not find object.";

        #endregion

        #region Construction

        public NotFoundException()
            : base(DefaultResponseMessage)
        {
                
        }

        public NotFoundException(string message)
            : base(message)
        {

        }

        public NotFoundException(string messgae, Exception innerException)
            : base(messgae, innerException)
        {

        }

        #endregion
    }
}

﻿using System;
using System.Net;

namespace covnet.Shared.Exceptions
{
    public class UnauthorizedException : BaseCustomException
    {
        #region Properties

        public override int StatusCode { get; } = (int)HttpStatusCode.Forbidden;

        public override string StatusMessage { get; } = "A permission error occurred";

        private static string DefaultResponseMessage =
            "User does not have access to object.";

        #endregion

        #region Construction

        public UnauthorizedException()
            : base(DefaultResponseMessage)
        {
                
        }

        public UnauthorizedException(string message)
            : base(message)
        {

        }

        public UnauthorizedException(string messgae, Exception innerException)
            : base(messgae, innerException)
        {

        }

        #endregion
    }
}

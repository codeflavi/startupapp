﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;

namespace covnet.Shared
{
    public class InternalServerErrorResult : ObjectResult
    {

        #region Properties

        public Exception Exception { private set; get; }       

        #endregion


        #region Construction

        public InternalServerErrorResult(
            object value,
            Exception exception) : base(value)
        {

            StatusCode = (int)HttpStatusCode.InternalServerError;
            Exception = exception;
        }

        #endregion
    }
}

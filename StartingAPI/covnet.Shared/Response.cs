﻿using Newtonsoft.Json;

namespace covnet.Shared
{
    /*
     * This class complied with the former Java backend API, hence it should be 
     * used only in end-points migrated from Java back-end
     * 
     */
    public class Response<T>
    {
        #region Constants

        private const string DEFAULT_STATUS = "success";

        #endregion

        #region Properties

        [JsonProperty(PropertyName = "status")]
        public string Status { get; } = DEFAULT_STATUS;

        [JsonProperty(PropertyName = "data")]
        public T Data { get; set; }

        #endregion
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;

namespace covnet.Shared
{
    public class ResponseHelper<T>
    {
        #region Returns Just The Object to Support Current API Behavior

        public static ObjectResult CreateResponse(T obj)
        {
            return new OkObjectResult(obj);
        }

        public static ObjectResult CreateResponse(IEnumerable<T> obj)
        {
            return new OkObjectResult(obj);
        }

        public static ActionResult CreateResponse(
           T obj,
           HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.Accepted:
                    return new AcceptedResult("", obj);
                case HttpStatusCode.NoContent:
                    return new NoContentResult();
                default:
                    return new OkObjectResult(obj);
            }
        }
        
        #endregion

        #region Returns a Response Object to Support further Java Migration

        public static ObjectResult CreateOkResponse(T obj)
        {
            Response<T> response = new Response<T>
            {
                Data = obj
            };
            return new OkObjectResult(response);
        }

        public static ObjectResult CreateOkResponse(IEnumerable<T> obj)
        {
            Response<IEnumerable<T>> response = new Response<IEnumerable<T>>
            {
                Data = obj
            };
            return new OkObjectResult(response);
        }

        //TODO: Implement returning other 2xx status codes
        public static ActionResult CreateOkResponse(
            HttpStatusCode statusCode)
        {
            switch (statusCode)
            {
                case HttpStatusCode.Accepted:
                    return new AcceptedResult();

                case HttpStatusCode.NoContent:
                    return new NoContentResult();

                default:
                    return new OkResult();
            }
        }

        public static ActionResult CreateOkResponse(
            T obj,
            HttpStatusCode statusCode)
        {
            Response<T> response = new Response<T>
            {
                Data = obj
            };
            switch (statusCode)
            {
                case HttpStatusCode.Accepted:
                    return new AcceptedResult("", response);

                case HttpStatusCode.NoContent:
                    return new NoContentResult();

                default:
                    return new OkObjectResult(response);
            }
        }

        public static ActionResult CreateOkResponse(
            IEnumerable<T> obj,
            HttpStatusCode statusCode)
        {
            Response<IEnumerable<T>> response = new Response<IEnumerable<T>>
            {
                Data = obj
            };
            switch (statusCode)
            {
                case HttpStatusCode.Accepted:
                    return new AcceptedResult("", response);

                case HttpStatusCode.NoContent:
                    return new NoContentResult();

                default:
                    return new OkObjectResult(response);
            }
        }

        #endregion
    }
}
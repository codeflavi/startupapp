﻿using Microsoft.AspNetCore.Http;

namespace covnet.Shared
{
    public class ServiceHelper
    {

        public static T GetService<T>(
            HttpContext httpContext)
        {
            return (T)httpContext
                .RequestServices
                .GetService(typeof(T));
        }


    }
}

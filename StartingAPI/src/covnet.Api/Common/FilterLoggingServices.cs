﻿using Microsoft.AspNetCore.Http;

namespace covet.Common
{
    public class FilterLoggingServices
    {

        public static T GetService<T>(
            HttpContext httpContext)
        {
            return (T)httpContext
                .RequestServices
                .GetService(typeof(T));
        }


    }
}

﻿using System.Collections.Generic;
using System.Threading.Tasks;
using covnet.AAA.Contracts;
using covnet.BusinessService.Contracts;
using covnet.Models.EfCoreModels;
using covnet.Shared;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace covnet.Api.Controllers
{
    [ApiController]
    [ApiVersion("2.0")]
    [Produces("application/json")]
    [Route("api/v{version:apiVersion}/Users")]
    [ResponseCache(CacheProfileName = "Never")]
    public class UserController : Controller
    {

        #region Fields
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        #endregion

        #region Contructors
        public UserController(
            IUserService service,
            ILogger<UserController> logger,
            IAuthenticationAndAuthorization authenticationAndAuthorization)
        {
            _logger = logger;
            _userService = service;
        }
        #endregion

        #region Methods
        [HttpGet()]
        public async Task<IActionResult> GetAllUsersAsync()
        {
            IEnumerable<User> users = await _userService.GetAllAsync();

            return ResponseHelper<User>.CreateResponse(users);
        }
        #endregion

    }
}
﻿using covet.Common;
using covnet.Shared;
using covnet.Shared.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace covnet.Middleware
{
    public class ExceptionMiddleware
    {
        #region Constants

        private static readonly HttpStatusCode DEFAULT_STATUS_CODE = HttpStatusCode.InternalServerError;
        private static readonly string DEFAULT_MESSAGE = "Unexpected error occurred";

        #endregion

        #region Fields

        private readonly RequestDelegate next;

        #endregion

        #region Construction

        public ExceptionMiddleware(
            RequestDelegate next)
        {
            this.next = next;
        }

        #endregion

        #region Methods

        public async Task Invoke(
            HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (CustomInternalServerException e)
            {
                await HandleExceptionAsync(context, e);
            }
            catch (BaseCustomException e)
            {
                await HandleExceptionAsync(context, e);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(
            HttpContext context,
            BaseCustomException exception)
            => HandleIt(
                context,
                exception,
                exception.StatusCode,
                new ErrorResponse(
                    message: exception.StatusMessage,
                    errors: new List<ErrorItem>()
                    {
                        new ErrorItem
                        (
                            code: exception.StatusCode.ToString(),
                            message: BuildErrorMessage(exception)
                        )
                    }));

        private static Task HandleExceptionAsync(
            HttpContext context,
            CustomInternalServerException exception)
            => HandleIt(
                context,
                exception,
                exception.StatusCode,
                new ErrorResponse(
                    message: exception.StatusMessage,
                    errors: new List<ErrorItem>()
                    {
                        new ErrorItem
                        (
                            code: exception.CustomCode.ToString(),
                            message: BuildErrorMessage(exception)
                        )
                    }));

        private static Task HandleExceptionAsync(
            HttpContext context,
            Exception exception)
            => HandleIt(
                context,
                exception,
                (int)DEFAULT_STATUS_CODE,
                new ErrorResponse(
                message: DEFAULT_MESSAGE,
                errors: new List<ErrorItem>()
                {
                    new ErrorItem
                    (
                        code: DEFAULT_STATUS_CODE.ToString(),
                        message: BuildErrorMessage(exception)
                    )
                }));
        

        private static string BuildErrorMessage(Exception ex)
            => (ex.InnerException != null) ? ex.Message + "; nested exception is " + ex.InnerException.Message : ex.Message;

        private static Task HandleIt(
            HttpContext context,
            Exception exception,
            int statusCode,
            ErrorResponse errorResponse)
        {
            var loggingService = FilterLoggingServices.GetService<
                ILogger<ExceptionMiddleware>>(context);

            loggingService?.LogCritical(
                exception,
                $"The error '{exception.Message}' with identifier '{errorResponse.identifier}' " +
                $"occured with the following trace : {exception.StackTrace}");

            var result = JsonConvert.SerializeObject(errorResponse);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;
            return context.Response.WriteAsync(result);
        }
        #endregion

    }
}

using covnet.AAA;
using covnet.AAA.Contracts;
using covnet.BusinessService;
using covnet.BusinessService.Contracts;
using covnet.Middleware;
using covnet.Models.EFCoreModels;
using covnet.Repository;
using covnet.Repository.Contracts;
using FluentNHibernate.Automapping;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace covnet.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            #region Transaction Manager

            //look into using AddDbContextPool
            services.AddDbContext<CovnetContext>(ServiceLifetime.Scoped);

            services.AddScoped<
                DbContext,
                CovnetContext>();

            services.AddScoped<
                ITransactionManagerInternal,
                TransactionManager>();

            services.AddScoped<
                ITransactionManager>(ctx => ctx.GetRequiredService<ITransactionManagerInternal>());

            #endregion

            #region Services Injection
            services.AddTransient<
                IUserService,
                UserService>();
            #endregion

            #region Repositories Injection
            services.AddTransient<
                IUserRepository,
                UserRepository>();
            #endregion

            services.AddScoped<
                IAuthenticationAndAuthorization, 
                AuthenticationAndAuthorization>();

            services.AddHttpContextAccessor();

            //Allow us to use IHttpClientFactory 
            services.AddHttpClient();

            //boilerplate code with additional caching/serialization options

            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
                options.CacheProfiles.Add("Never",
                    new CacheProfile()
                    {
                        Location = ResponseCacheLocation.None,
                        NoStore = true
                    });
            })
           .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
           .AddNewtonsoftJson(options =>
           {
               options.SerializerSettings.ContractResolver = new DefaultContractResolver();
               //options.SerializerSettings.MaxDepth = 1;
               //this ensures that only one level of recursion is used
               //e.g getting a user with organisations
               //an organisation also has a users property, which in turn has an organisations property........
               options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

               options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;

           });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(
                    "v2",
                    new OpenApiInfo
                    {
                        Title = "Covnet.API",
                        Description = "Covnet.API",
                        Version = "v2"
                    });
            });

            //api versioning

            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = true;
                o.AssumeDefaultVersionWhenUnspecified = true;
                o.DefaultApiVersion = new ApiVersion(2, 0);
                o.ApiVersionReader = new UrlSegmentApiVersionReader();
            });

            ConfigureMappings(services);

            // TO DO: FF Add CORS

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, 
            IWebHostEnvironment env, 
            IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(
                    "/swagger/v2/swagger.json",
                    "covnet.Api");
            });

            app.UseMiddleware(typeof(ExceptionMiddleware));
        }

        public static void ConfigureMappings(IServiceCollection services)
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
               
            });
        }
    }
}
